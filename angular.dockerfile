FROM node:lts

WORKDIR /usr/src/app/angular
COPY angular/package.json angular/package-lock.json ./
RUN npm ci
COPY ./angular/ .
EXPOSE 4201 49153
ENTRYPOINT npm run start:docker
