import express from 'express';
import cors from 'cors';
import { routes } from './routes';
import passport from 'passport';
import JwtStrategy from 'passport-jwt';
import environment from './environment';

export const app = express();

const strategyOptions = {} as JwtStrategy.StrategyOptions;
strategyOptions.issuer = environment.app.name;
strategyOptions.secretOrKey = environment.authentication.secret_key;
strategyOptions.audience = environment.app.domain;
strategyOptions.jwtFromRequest = JwtStrategy.ExtractJwt.fromAuthHeaderAsBearerToken();
const strategy = new JwtStrategy.Strategy(strategyOptions, (token, done) => {
    console.log('Strategy output:', token);
    if (!token.sub) {
        done(new Error('sub not found in token'));
    }
    return done(undefined, token);
});
passport.use(strategy);

app.use(express.json());
app.use(cors());
app.use('/api/', routes);
app.use(passport.initialize());
