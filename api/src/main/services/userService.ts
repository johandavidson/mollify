import { QueryResult, sql } from 'slonik';
import DBController from '../controllers/databaseController';

class UserService {
    async createUser(email: string, password: string, name?: string): Promise<{ id: string, email: string, name: string }> {
        return DBController.pool.one<{ id: string, email: string, name: string }>(sql`
            INSERT INTO mollify_user (email, name, password) 
            VALUES (${email}, ${name || null}, crypt(${password}, gen_salt('bf', 8))) 
            ON CONFLICT (email) DO NOTHING
            RETURNING id, email, name`);
    }
    
    async getUserByEmail(id: string): Promise<{ id: string, email: string, name: string }> {
        return DBController.pool.one(sql`SELECT id as sub, name, email FROM mollify_user WHERE id = ${id}`);
    }

    async getUserByEmailAndPassword(email: string, password: string): Promise<{ id: string, email: string, name: string }> {
        return DBController.pool.one(sql`SELECT id as sub, name, email FROM mollify_user WHERE email = ${email} AND password = crypt(${password}, password)`);
    }
}
export default new UserService();
