import { sql } from 'slonik';
import DBController from '../controllers/databaseController';
import UserService from '../services/userService';

class SetupService {
    get isNeeded(): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            return DBController.pool.query<boolean>(sql`SELECT * FROM mollify_user LIMIT 1;`).then(result => {
                resolve(result.rowCount === 0);
            });
        });
    }
}
export default new SetupService();
