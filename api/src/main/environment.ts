import { cleanEnv, str, bool, num } from 'envalid';

const env = cleanEnv(process.env, {
    MOLLIFY_JWT_SECRET: str({ desc: 'Secret key used for signing the JWT token' }),
    MOLLIFY_DOMAIN: str({ desc: 'The domain Mollify will be running on', default: 'mollify.se'}),
    MOLLIFY_DB_URL: str(),
    MOLLIFY_DB_PORT: num(),
    MOLLIFY_DB_USERNAME: str(),
    MOLLIFY_DB_PASSWORD: str()
});

export default {
    app: {
        name: process.env.npm_package_name,
        version: process.env.npm_package_version,
        domain: env.MOLLIFY_DOMAIN
    },
    server: {
        port: 9000
    },
    authentication: {
        secret_key: env.MOLLIFY_JWT_SECRET
    },
    postgres: {
        url: env.MOLLIFY_DB_URL,
        port: env.MOLLIFY_DB_PORT,
        username: env.MOLLIFY_DB_USERNAME,
        password: env.MOLLIFY_DB_PASSWORD
    }
}