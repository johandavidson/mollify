import environment from '../environment';
import { createPool, sql, DatabasePool } from 'slonik';
import { env } from 'process';

export class DBController {
    private _pool?: DatabasePool;
    get pool(): DatabasePool {
        if (!this._pool) {
            this._pool = createPool(`postresql://${environment.postgres.username}:${environment.postgres.password}@${environment.postgres.url}:${environment.postgres.port}/mollify`);
        }
        return this._pool;
    }
    async init(): Promise<void> {
        const dbstatus = await this.pool.query(sql`SELECT * FROM pg_catalog.pg_tables WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema'`);
        if (dbstatus.rowCount === 0) {
            await this.setup();
        }
    }
    async setup(): Promise<void> {
        console.log('DatabaseController:setup: Setting up tables');
        Promise.all([
            this.createUserTable()
        ]).then(() => {
            console.log('DatabaseController:setup: Tables created');
        }).catch(err => {
            console.error(err);
        });
    }
    
    async seed(): Promise<void> {
        if (env.NODE_ENV === "development") {
            await this.pool.query(sql`INSERT INTO mollify_user (email, name, password) VALUES ('test@test.com', 'test testson', crypt('12345', gen_salt('bf', 8))) ON CONFLICT (email) DO NOTHING`)
        } else {
            throw new Error('Unable to seed database when in production');
        }
    }

    private async createUserTable(): Promise<void> {
        return this.pool.query(sql`CREATE TABLE IF NOT EXISTS mollify_user (
            id uuid NOT NULL DEFAULT gen_random_uuid() PRIMARY KEY,
            email text NOT NULL UNIQUE,
            password text NOT NULL,
            name text NOT NULL
        )`).then(() => {
            console.log('DatabaseController:setup: Created user table');
        });
    }
}
export default new DBController();
