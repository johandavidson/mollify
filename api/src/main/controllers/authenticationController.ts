import { Request, Response, NextFunction } from 'express';
import Passport from 'passport';

export const authenticate = (req: Request, res: Response, next: NextFunction): void => {
    return Passport.authenticate(
        'jwt',
        {
            session: false
        }
    )(req, res, next);
};
