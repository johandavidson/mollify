import environment from '../environment';

export const getVersion = (): { version: { build: string, commit: string, package: string } } => ({
    version: {
        build: process.env.BUILD_ID || 'n/a',
        commit: process.env.GIT_COMMIT || 'n/a',
        package: `${environment.app.name} ${environment.app.version}`
    }
});
