import express, { Request, Response } from 'express';
import UserService from '../services/userService';

export const routes = express.Router();

routes.get('/:email', async (req: Request, res: Response) => {
    const email = req.params.email;
    console.log(`index: GET /user/${email}`);
    const user = await UserService.getUserByEmail(email);
    res.json(user);
});
