import express, { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import UserService from '../services/userService';
import environment from '../environment';

export const routes = express.Router();

routes.post('/signin', async (req: Request, res: Response) => {
    console.log('index: POST /signin');
    const email = req.body.email;
    const password = req.body.password;
    try {
        const user = await UserService.getUserByEmailAndPassword(email, password);
        const signOptions = {} as jwt.SignOptions;
        signOptions.audience = environment.app.domain;
        signOptions.issuer = environment.app.name;
        res.json({ token: jwt.sign(user, environment.authentication.secret_key, signOptions) });
    } catch {
        res.status(401).send();
    }
});
