import express, { Request, Response } from 'express';
import environment from '../environment';
import { getVersion } from '../controllers/versionController';
import { routes as authenticationRoutes } from './authenticationRoutes';
import { routes as userRoutes } from './userRoutes';
import setupRoutes from './setupRoutes';
import { authenticate } from '../controllers/authenticationController';
import DBController from '../controllers/databaseController';

export const routes = express.Router();

routes.use('/authentication', authenticationRoutes);
routes.use('/user', authenticate, userRoutes);
routes.use('/setup', setupRoutes);

routes.get('/', (req: Request, res: Response) => {
    console.log('index: GET /');
    res.send(`Hello from ${environment.app.name} (${environment.app.version})`);
});
routes.get('/version', (req: Request, res: Response) => {
    console.log('index: GET /version');
    res.json(getVersion());
});
routes.post('/seed', async (req: Request, res: Response) => {
    console.log('index: POST /seed');
    try {
        await DBController.seed();
        res.json({ status: 'Seed complete.'});
    } catch {
        res.json({ error: 'Unable to seed database.'});
    }
});
