import express, { Request, Response } from 'express';
import setupService from '../services/setupService';
import UserService from '../services/userService';

const routes = express.Router();

routes.get('/', async (req: Request, res: Response) => {
    console.log('setupRoutes: GET /');
    res.json(await setupService.isNeeded);
});

routes.post('/user', async (req: Request, res: Response) => {
    console.log('setupRoutes: POST /user');

    const email = req.body.email;
    const name = req.body.name;
    const password = req.body.password;

    res.json(UserService.createUser(email, password, name));
});

export default routes;
