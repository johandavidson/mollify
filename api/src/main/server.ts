import http from 'http';
import { app } from './app';
import environment from './environment';
import DBController from './controllers/databaseController';

const port = environment.server.port || 9000;

const server = http.createServer(app);
DBController.init().then(() => {
    server.listen(port, () => {
        console.log(`Server running on port ${port}`);
    });
});
