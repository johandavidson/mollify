import { HttpErrorResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { User } from '../models/user';

import { AuthenticationService } from './authentication.service';
const testtoken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI5YWFmMDVmMy03MjFlLTQ0NDYtYjRiZS02YTZkNTUyYTc3ZDIiLCJuYW1lIjoiSm9obiBEb2UiLCJpYXQiOjE1MTYyMzkwMjJ9.2-Nj3Piu5RdBDOAus7uzgA_-UQCMrstQ6siSRT0Weu4';

describe('AuthenticationService', () => {
  let service: AuthenticationService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule, RouterTestingModule ]
    });
    service = TestBed.inject(AuthenticationService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('SignIn', () => {
    it('should call the api', (done) => {
      // given
      const user = {} as User;
      const token = { token: testtoken };

      // when
      service.signIn(user).subscribe(response => {
        expect(response.status).toEqual(200);
        expect(response.body).toEqual(token);
        done();
      });
      const signinRequest = httpMock.expectOne('/api/authentication/signin');
      
      // then
      expect(signinRequest.request.method).toEqual('POST');
      signinRequest.flush(token);

      const userRequest = httpMock.expectOne(() => true);
      expect(userRequest.request.method).toEqual('GET');
      userRequest.flush(user);
    });

    it('should handle if api returns empty response', (done) => {
      // given
      const user = {} as User;
      const spy = spyOn(window.sessionStorage, 'setItem');

      // when
      service.signIn(user).subscribe({
        next: response => {
        },
        error: (err: HttpErrorResponse) => {
          console.log(err);
          expect(err.status).toEqual(400);
          expect(spy).toHaveBeenCalledTimes(0);
          done();
        }
      });
      const signinRequest = httpMock.expectOne('/api/authentication/signin');
      
      // then
      expect(signinRequest.request.method).toEqual('POST');
      signinRequest.flush({}, { status: 400, statusText: 'Bad request'});

      httpMock.expectNone(() => true);
    });
  });

  describe('isLoggedIn', () => {
    it('should responde true if sessionStorage has a token', () => {
      // given
      spyOn(window.sessionStorage, 'getItem').and.returnValue('token');

      // then
      expect(service.isLoggedIn).toBeTruthy();
    });

    it('should responde false if sessionStorage doesnt have a token', () => {
      // given
      spyOn(window.sessionStorage, 'getItem').and.returnValue(null);

      // then
      expect(service.isLoggedIn).toBeFalsy();
    })
  });

  describe('getToken', () => {
    it('should return token', () => {
      // given
      const token = 'token';
      spyOn(window.sessionStorage, 'getItem').and.returnValue(token);

      // then
      expect(service.getToken()).toEqual(token);
    });

    it('should return an empty string if no token present', () => {
      // given
      spyOn(window.sessionStorage, 'getItem').and.returnValue(null);

      // then
      expect(service.getToken()).toEqual('');
    });
  });

  describe('ngOnInit', () => {
    it('should init the service', fakeAsync(() => {
      // given
      const dto = { email: 'test@test.com' }
      const user = new User(dto);
      spyOn(window.sessionStorage, 'getItem').and.returnValue(testtoken);

      // when
      service.ngOnInit();

      // then
      const req = httpMock.expectOne(() => true);
      req.flush(dto);
      expect(req.request.method).toEqual('GET');
      tick();
      expect(service.currentUser).toEqual(user);
    }));
  });

  describe('handleError', () => {
    it('should handle server side errors', (done) => {
      // given
      const error = { status: 400, message: 'Bad request' } as HttpErrorResponse;

      // when
      service.handleError(error).subscribe({
        error: err => {
          expect(err.message).toEqual(`Error Code: ${error.status}\nMessage: ${error.message}`);
          done();
        }
      });
    });

    it('should handle client side errors', (done) => {
      // given
      const error = { error: new ErrorEvent('Bad request') } as HttpErrorResponse;

      // when
      service.handleError(error).subscribe({
        error: err => {
          expect(err.message).toEqual(error.error.message);
          done();
        }
      });
    });
  })
});
