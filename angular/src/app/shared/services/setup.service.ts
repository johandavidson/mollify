import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class SetupService {

  constructor(
    private http: HttpClient
  ) { }

  get isSetupNeeded(): Observable<boolean> {
    return this.http.get<boolean>(`${environment.api.endpoint}/setup`).pipe(
      map(response => response),
      catchError(err => of(true))
    );
  }

  createFirstUser(user: User): Observable<User> {
    return this.http.post<User>(`${environment.api.endpoint}/setup/user`, user, { observe: 'body' });
  }
}
