import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { User, UserDTO } from '../models/user';
import { environment } from 'src/environments/environment';
import { catchError, map, Observable, tap, throwError } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService implements OnInit {
  currentUser?: User;
  helper = new JwtHelperService();

  constructor(
    private http: HttpClient,
  ) { }

  ngOnInit(): void {
    const id = this.helper.decodeToken(this.getToken()).sub;
    if (id) {
      this.getUserProfile(id).subscribe(response => this.currentUser = response)
    }
  }

  signIn(user: User): Observable<HttpResponse<{ token: string }>> {
    return this.http.post<{ token: string }>(`${environment.api.endpoint}/authentication/signin`, user, { observe: 'response'})
      .pipe(
        tap(response => {
          if (response.status === 200 && response.body) {
            sessionStorage.setItem('access_token', response.body.token);
            this.getUserProfile(this.helper.decodeToken(response.body.token).sub).subscribe(userProfileResponse => {
              this.currentUser = userProfileResponse;
            });
          }
        })
      );
  }

  getUserProfile(sub: string): Observable<User> {
    return this.http.get<UserDTO>(`${environment.api.endpoint}/user/${encodeURIComponent(sub)}`).pipe(
      map(response => {
        return new User(response);
      }),
      catchError(this.handleError)
    )
  }

  get isLoggedIn(): boolean {
    const token = sessionStorage.getItem('access_token');
    return (token) ? true: false;
  }

  getToken(): string {
    return sessionStorage.getItem('access_token') || '';
  }

  handleError(error: HttpErrorResponse) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {
      // client side error
      msg = error.error.message;
    } else {
      // server side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(() => new Error(msg));  }
}
