import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of, throwError } from 'rxjs';
import { User } from '../models/user';

import { SetupService } from './setup.service';

describe('SetupService', () => {
  let service: SetupService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ]
    });
    service = TestBed.inject(SetupService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('isSetupNeeded', () => {
    it('should responde true if api responds true', () => {
      // when
      service.isSetupNeeded.subscribe(result => {
        expect(result).toBeTruthy();
      });

      const req = httpMock.expectOne('/api/setup');
      req.flush(true);
      expect(req.request.method).toEqual('GET');
    });

    it('should responde true if api fails', () => {
      // when
      service.isSetupNeeded.subscribe(result => {
        expect(result).toBeTruthy();
      });

      const req = httpMock.expectOne('/api/setup');
      req.flush({}, { status: 400, statusText: 'Bad request'});
      expect(req.request.method).toEqual('GET');
    });

    it('should responde false if api responds false', () => {
      // when
      service.isSetupNeeded.subscribe(result => {
        expect(result).toBeFalsy();
      });

      const req = httpMock.expectOne('/api/setup');
      req.flush(false);
      expect(req.request.method).toEqual('GET');
    });
  });

  describe('createFirstUser', () => {
    it('should send a request to the api', () => {
      // given
      const user = new User({});

      // when
      service.createFirstUser(user).subscribe(response => {
        expect(response).toEqual(user);
      });
      const req = httpMock.expectOne('/api/setup/user');
      req.flush(user);
      expect(req.request.method).toEqual('POST');

      // then
    });
  });
});
