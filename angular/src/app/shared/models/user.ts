export class User {
    _id: string;
    name?: string;
    email: string;
    password?: string;
    constructor(user: { sub?: string, name?: string, email?: string, password?: string }) {
        this._id = user.sub || '';
        this.name = user.name;
        this.email = user.email || '';
        this.password = user.password;
    }
}

export interface UserDTO {
    sub: string;
    name: string;
    email: string;
}
