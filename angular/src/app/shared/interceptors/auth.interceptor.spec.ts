import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { AuthenticationService } from '../services/authentication.service';

import { AuthInterceptor } from './auth.interceptor';

describe('AuthInterceptor', () => {
  let client: HttpClient;
  let controller: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
        { provide: AuthenticationService, useValue: { getToken: () => 'token' } }
      ]
    });

    client = TestBed.inject(HttpClient);
    controller = TestBed.inject(HttpTestingController);
  });

  it('should add authorization header', (done) => {
    client.get('/test').subscribe(response => {
      expect(response).toEqual({});
      done();
    });

    const request = controller.expectOne('/test');
    expect(request.request.headers.has('Authorization')).toBeTruthy();
    expect(request.request.headers.get('Authorization')).toEqual('Bearer token')
    request.flush({});
  });
});
