import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable, of } from 'rxjs';
import { SetupService } from '../services/setup.service';

import { SetupGuard } from './setup.guard';

describe('SetupGuard', () => {
  let guard: SetupGuard;
  const mockRouter = {
    createUrlTree: jasmine.createSpy('createUrlTree')
  };
  const setupService = jasmine.createSpyObj<SetupService>('SetupService', [], [ 'isSetupNeeded' ]);
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ 
        SetupGuard,
        { provide: Router, useValue: mockRouter },
        { provide: SetupService, useValue: setupService }
      ],
      imports: [ HttpClientTestingModule, RouterTestingModule ]
    });
    guard = TestBed.inject(SetupGuard);
    router = TestBed.inject(Router);
  });

  afterEach(() => {
    mockRouter.createUrlTree = jasmine.createSpy('createUrlTree');
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  describe('when the setup is not needed', () => {
    beforeEach(() => {
      (Object.getOwnPropertyDescriptor(setupService, 'isSetupNeeded')?.get as jasmine.Spy<() => Observable<boolean>>).and.returnValue(of(false));
    });

    it('should redirect the user to the dashboard', fakeAsync(() => {
      // given

      // when
      const canActivateResult = (guard.canActivate({} as ActivatedRouteSnapshot, {} as RouterStateSnapshot) as Observable<boolean | UrlTree>);
      tick();

      // then
      canActivateResult.subscribe((result) => {
        expect(result).toBeFalsy();
        expect(mockRouter.createUrlTree).toHaveBeenCalledWith(['dashboard']);
      });
    }));
  });

  describe('when the setup is needed', () => {
    beforeEach(() => {
      (Object.getOwnPropertyDescriptor(setupService, 'isSetupNeeded')?.get as jasmine.Spy<() => Observable<boolean>>).and.returnValue(of(true));
    });

    it('should return true', fakeAsync(() => {
      // given

      // when
      const canActivateResult = (guard.canActivate({} as ActivatedRouteSnapshot, {} as RouterStateSnapshot) as Observable<boolean | UrlTree>);
      tick();

      // then
      canActivateResult.subscribe((result) => {
        expect(result).toBeTruthy();
        expect(mockRouter.createUrlTree).toHaveBeenCalledTimes(0);
      });
    }));
  })

});
