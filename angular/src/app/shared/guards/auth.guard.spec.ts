import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

import { AuthGuard } from './auth.guard';

describe('AuthGuard', () => {
  const mockRouter = {
    createUrlTree: jasmine.createSpy('createUrlTree')
  };
  const authService = jasmine.createSpyObj<AuthenticationService>('AuthenticationService', [], [ 'isLoggedIn' ]);
  let guard: AuthGuard;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthGuard,
        { provide: Router, useValue: mockRouter },
        { provide: AuthenticationService, useValue: authService }
      ],
    }).compileComponents();
    guard = TestBed.inject(AuthGuard);
    router = TestBed.inject(Router);
  });

  describe('when the user is logged in', () => {
    beforeEach(() => {
      (Object.getOwnPropertyDescriptor(authService, 'isLoggedIn')?.get as jasmine.Spy<() => boolean>).and.returnValue(true);
    });

    it('should allow the authenticated user to access the route', () => {
      expect(guard.canActivate({} as ActivatedRouteSnapshot, {} as RouterStateSnapshot)).toBeTruthy();
    });
  })

  describe('when the user is not logged in', () => {
    beforeEach(() => {
      (Object.getOwnPropertyDescriptor(authService, 'isLoggedIn')?.get as jasmine.Spy<() => boolean>).and.returnValue(false);
    })
    it('should redirect an unauthenticated user to the login route', () => {
      expect(guard.canActivate({} as ActivatedRouteSnapshot, {} as RouterStateSnapshot)).toBeFalsy();
      expect(mockRouter.createUrlTree).toHaveBeenCalledWith(['/login']);
    });
  })
});
