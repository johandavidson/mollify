import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { LoginComponent } from './modules/login/login.component';
import { SetupComponent } from './modules/setup/setup.component';
import { AuthGuard } from './shared/guards/auth.guard';
import { SetupGuard } from './shared/guards/setup.guard';

const routes: Routes = [
  { path: '', redirectTo: '/setup', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule), canActivate: [ AuthGuard ] },
  { path: 'setup', component: SetupComponent, canActivate: [ SetupGuard ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
