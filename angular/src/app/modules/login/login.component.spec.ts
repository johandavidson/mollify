import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, throwError } from 'rxjs';
import { User } from 'src/app/shared/models/user';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';

import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let signInSpy = jasmine.createSpy('signIn').and.returnValue(of(true));
  let navigateSpy = jasmine.createSpy('navigate');

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      providers: [
        { provide: AuthenticationService, useValue: { signIn: signInSpy }},
        { provide: Router, useValue: { navigate: navigateSpy }}
      ],
      imports: [ 
        ReactiveFormsModule,
        RouterTestingModule
      ]
    })
    .compileComponents();
  });

  afterEach(() => {
    signInSpy = jasmine.createSpy('signIn').and.returnValue(of(true));
    navigateSpy = jasmine.createSpy('navigate');
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
    
  it('should have a password field and an email field', () => {
    // given
    const emailElement = fixture.debugElement.query(By.css('#email'));
    const passwordElement = fixture.debugElement.query(By.css('#password'));

    // when
    // then
    expect(emailElement).toBeTruthy();
    expect(passwordElement).toBeTruthy();
  });

  describe('onSubmit', () => {
    it('should handle submit', () => {
      // given
      const formdata = { email: 'test@test.com', password: '12345' };
      const user = new User(formdata);
      // when
      component.onSubmit(formdata);

      // then
      expect(signInSpy).toHaveBeenCalledWith(user);
      expect(navigateSpy).toHaveBeenCalledWith(['dashboard']);
    });

    it('should handle error', () => {
      // given
      signInSpy.and.returnValue(throwError(() => new Error('error')));
      const formdata = { email: 'test@test.com', password: '12345' };
      const user = new User(formdata);
      // when
      component.onSubmit(formdata);

      // then
      expect(signInSpy).toHaveBeenCalledWith(user);
      expect(navigateSpy).toHaveBeenCalledTimes(0);
    });
  });
});
