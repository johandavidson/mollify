import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/shared/models/user';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  formData = new FormGroup({
    email: new FormControl(),
    password: new FormControl()
  });
  error?: string;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) { }

  onSubmit(data: { email: string, password: string }): void {
    this.authenticationService.signIn(new User(data)).subscribe({
      next: async () => {
        await this.router.navigate(['dashboard']);
      },
      error: err => {
        this.error = $localize`Login failed. Please try again.`;
      }
    });
  }
}
