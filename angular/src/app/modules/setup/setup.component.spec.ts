import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, throwError } from 'rxjs';
import { User } from 'src/app/shared/models/user';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { SetupService } from 'src/app/shared/services/setup.service';

import { SetupComponent } from './setup.component';

describe('SetupComponent', () => {
  let component: SetupComponent;
  let fixture: ComponentFixture<SetupComponent>;
  let signInSpy = jasmine.createSpy('signIn').and.returnValue(of(true));
  let createFirstUserSpy = jasmine.createSpy('createFirstUser').and.returnValue(of({} as User));
  let navigateSpy = jasmine.createSpy('navigate');

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetupComponent ],
      imports: [ HttpClientTestingModule, RouterTestingModule, ReactiveFormsModule ],
      providers: [
        { provide: SetupService, useValue: { createFirstUser: createFirstUserSpy }},
        { provide: AuthenticationService, useValue: { signIn: signInSpy }},
        { provide: Router, useValue: { navigate: navigateSpy }}
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    signInSpy = jasmine.createSpy('signIn').and.returnValue(of(true));
    navigateSpy = jasmine.createSpy('navigate');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a form with three fields', () => {
    // given
    const emailElement = fixture.debugElement.query(By.css('#email'));
    const passwordElement = fixture.debugElement.query(By.css('#password'));
    const nameElement = fixture.debugElement.query(By.css('#name'));

    // when
    // then
    expect(emailElement).toBeTruthy();
    expect(passwordElement).toBeTruthy();
    expect(nameElement).toBeTruthy();
  });

  describe('onSubmit', () => {
    it('should handle submit', () => {
      // given
      const formdata = { email: 'test@test.com', password: '12345', name: 'test testsson' };
      const user = new User(formdata);
      createFirstUserSpy.and.returnValue(of(user));
      // when
      component.onSubmit(formdata);

      // then
      expect(createFirstUserSpy).toHaveBeenCalledWith(user);
      expect(signInSpy).toHaveBeenCalledWith(user);
      expect(navigateSpy).toHaveBeenCalledWith(['dashboard']);
      expect(component.error).toBeFalsy();
    });

    it('should handle error in setupservice', () => {
      // given
      createFirstUserSpy.and.returnValue(throwError(() => new Error('error')));
      const formdata = { email: 'test@test.com', password: '12345', name: 'test testsson' };
      const user = new User(formdata);
      // when
      component.onSubmit(formdata);

      // then
      expect(createFirstUserSpy).toHaveBeenCalledWith(user);
      expect(signInSpy).toHaveBeenCalledTimes(0);
      expect(component.error).toEqual('error');
    });

    it('should handle error in authenticationservice', () => {
      // given
      const formdata = { email: 'test@test.com', password: '12345', name: 'test testsson' };
      const user = new User(formdata);
      createFirstUserSpy.and.returnValue(of(user));
      signInSpy.and.returnValue(throwError(() => new Error('error')));

      // when
      component.onSubmit(formdata);

      // then
      expect(createFirstUserSpy).toHaveBeenCalledWith(user);
      expect(signInSpy).toHaveBeenCalledWith(user);
      expect(navigateSpy).toHaveBeenCalledTimes(0);
      expect(component.error).toEqual('error');
    });
  });
});
