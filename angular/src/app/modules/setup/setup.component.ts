import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User, UserDTO } from 'src/app/shared/models/user';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { SetupService } from 'src/app/shared/services/setup.service';

@Component({
  selector: 'app-setup',
  templateUrl: './setup.component.html',
  styleUrls: ['./setup.component.scss']
})
export class SetupComponent implements OnInit {
  formData = new FormGroup({
    email: new FormControl(),
    password: new FormControl(),
    name: new FormControl()
  });
  error?: string;

  constructor(
    private setupService: SetupService,
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit(): void {

  }

  onSubmit(data: { email: string, name: string, password: string}) {
    const user = new User(data);
    this.setupService.createFirstUser(user).subscribe({
      next: () => {
        this.authenticationService.signIn(user).subscribe({
          next: () => {
            this.router.navigate(['dashboard']);
          },
          error: err => {
            console.error(err);
            this.error = err.message;
          }
        });
      },
      error: err => {
        console.error(err);
        this.error = err.message;
      }
    });
  }
}
