import { Component, OnInit } from '@angular/core';
import { faHome, faClipboardList, faCog } from '@fortawesome/free-solid-svg-icons';
import { faUser, faBuilding } from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  faHome = faHome;
  faClipboardList = faClipboardList;
  faUser = faUser;
  faBuilding = faBuilding;
  faCog = faCog;

  constructor() { }

  ngOnInit(): void {
  }

}
