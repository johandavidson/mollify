module.exports = {
    testEnvironment: 'node',
    collectCoverage: true,
    roots: [
        '<rootDir>'
    ],
    modulePaths: [
        '<rootDir>/api/src/'
    ],
    modulePathIgnorePatterns: [
        '<rootDir>/mollify/'
    ],
    moduleDirectories: [
        'node_modules'
    ],
    transform: {
        '^.+\\.tsx?$': 'ts-jest'
    },
    testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$',
    'moduleFileExtensions': [
        'ts',
        'tsx',
        'js',
        'jsx',
        'json',
        'node'
    ],
    globalSetup: './api/src/test/setup-tests.ts'
};
