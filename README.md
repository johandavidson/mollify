# mollify

Mollify is an open source ticket system created with Angular/NodeJs/PostgreeSQL/Docker/Bootstrap.

## Installation
tba

## Support
Add an issue here on Gitlab and the team behind will take a closer look at it.

## Roadmap
If you have ideas for releases in the future you are welcome to suggest it. Add an issue and feel free to provide a Merge Request.

## Contributing
This project is open for contributions. Just knock on the door...

## Authors and acknowledgment
At the moment it is only me. Welcome!

## License
ISC

## Project status
This project is just started and it will take some time before it will be up and running.

