FROM node:lts-alpine

WORKDIR /usr/src/app/api
COPY ./api/ .
RUN npm ci

EXPOSE 9000
ENTRYPOINT npm run dev:api
